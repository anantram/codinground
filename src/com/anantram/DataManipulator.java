/**
 * @author anantram
 *
 */
package com.anantram;

import java.io.IOException;

public class DataManipulator 
{
	public static final String INPUT_FILE_PATH = "/src/atdd/challenge_tests.csv";
	
	/** Main method
	 * @param args
	 */
	public static void main(String[] args) 
	{
		StringManipulator sm = new StringManipulator();
		String[] inputData = null;
		try {
			inputData = sm.readFile(System.getProperty("user.dir") + INPUT_FILE_PATH);
			sm.sorting(inputData);
		} catch (Exception e) {
			System.out.println(String.join(" ", 
					"Exception occured while reading input File. Details:",
					e.getMessage()));
		}
		
		
	}
}
