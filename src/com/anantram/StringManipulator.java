/**
 * @author anantram
 *
 */
package com.anantram;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class StringManipulator {

	public static final int LOWER_ASCII_START = 97;
	public static final int LOWER_ASCII_END = 122;
	public static final int UPPER_ASCII_START = 65;
	public static final int UPPER_ASCII_END = 90;
	public static final int LOWER_TO_UPPER_ASCII_DIFF = 32;
	public static final int SIZE_OF_CHECKBIT_ARRAY = 26;
	public static final String OUTPUT_FILE_PATH = "/src/atdd/challenge_sorted.csv";
	public static final String OUTPUT_FILE_HEADER = "Word,Unique,Weight";
	
	/**
	 * 
	 * Class to store weight and uniqueness of each word.
	 *
	 */
	class WordData implements Comparable<WordData> {
		
		public final String wordValue;
		public final double weight;
		public final boolean isUnique;
		
		/**
		 * Constructor
		 * 
		 * @param inputWord input word
		 * @param hasUniqueChars value to keep uniqueness
		 * @param weight Weight of the word
		 */
		public WordData(String inputWord, boolean hasUniqueChars, double weight) {
			
			this.wordValue = inputWord;
			this.weight = weight;
			this.isUnique = hasUniqueChars;
		}

		/**
		 * Overriding compareTo function.
		 * Compares weight logic:
		 * check weight first. 
		 * If weight is same, then order string according to value. 
		 */
		@Override
		public int compareTo(WordData arg0) {
			
			if(this.weight > arg0.weight)
				return 1;
			else if(this.weight < arg0.weight)
				return -1;
			else 
				return this.wordValue.compareTo(arg0.wordValue);
		}
	}
	
	/**
	 * Cleans the string by removing non-alphabetical characters from the input string and
	 *  converts all characters to uppercase.
	 * 
	 * @param input Raw input string.
	 * 
	 * @return Returns a string with only uppercase alphabetic characters.
	 * 
	 */
	public String cleanString(String input) {
		
		// Using Regex replaced all character which are not alphabets.
		return input.replaceAll("[^a-zA-Z]", "").toUpperCase();
	}
	
	/**
	 * Checks if input string contains all unique alphabets.
	 * 
	 * @param inputString String containing input characters.
	 * 
	 * @return Returns true if all alphabets in the string are unique.
	 */
	public boolean hasUniqueChars(String inputString) {
		
		String cleanedString = cleanString(inputString);
		Set<Character> charSet = new HashSet<Character>();
		
		for (int currCharIndex = 0; currCharIndex < cleanedString.length(); currCharIndex++)
		{
			charSet.add(cleanedString.charAt(currCharIndex));
		}
		
		// Compare original string length vs unique list of chars pushed to Hashset. 
		return cleanedString.length() == charSet.size();
	}
	
	/**
	 * Returns the weight of input string.
	 * 
	 * @param inputString The input string.
	 * 
	 * @return Weight of the string
	 */
	public double getWeight(String inputString) {
		
		double sum = 0;
		String cleanedString = cleanString(inputString);
		
		// Adding Ascii values of all characters in the string.
		for (int currCharIndex = 0; currCharIndex < cleanedString.length(); currCharIndex++) {
			sum += (int)cleanedString.charAt(currCharIndex);
		}
		return sum / cleanedString.length();
	}
	
	/**
	 * Sort and stores all words in a csv file with corresponding weight and uniqueness.
	 * 
	 * @param words Raw inputs words.
	 * 
	 */
	public void sorting(String[] words) {
		
		ArrayList<WordData> inputWords = new ArrayList<WordData>();
		String clearedString = "";
		
		for(String word : words) {
			clearedString = cleanString(word);
			
			// Avoiding addition of empty words to the list.
			// Ex. cleanstring(#&^&6#:) will return an empty string. 
			if(0 < clearedString.length()) {
				
				// Creating WordData object to stored all details about the word.
				WordData newWord = new WordData(clearedString,
						hasUniqueChars(word),
						getWeight(word));
				
				// Adding the wordData object to list.
				inputWords.add(newWord);
			}
		}
		// Sorting the list.
		Collections.sort(inputWords);
		
		writeToCSV(inputWords, System.getProperty("user.dir") + OUTPUT_FILE_PATH);
	}
	
	/**
	 * Reads the file and returns the data as a string array.
	 * 
	 * @param path Path of the input file.
	 * 
	 * @return Array of words
	 * 
	 * @throws IOException Thows IOException if any error occurs while creating the file reader.
	 */
	public String[] readFile(String path) throws IOException {
		
		ArrayList<String> inputWords = new ArrayList<String>();
		BufferedReader inputFileReader = new BufferedReader(new FileReader(path));
		
		// Reading first line and discarding it.
		String inputWord = inputFileReader.readLine();
		
		// Start fetching rows
		inputWord = inputFileReader.readLine();
		
		while(null != inputWord) {
			
			// Skipping empty string rows.
			if (0 < inputWord.length())
				inputWords.add(inputWord);
			
			inputWord = inputFileReader.readLine();
		}
		
		// Closing filereader
		inputFileReader.close();
		return inputWords.toArray(new String[0]);
	}

	/**
	 * Writes data to a CSV file
	 * 
	 * @param resultData Data which has to be saved in the CSV file.
	 * @param outputPath Path of the output.
	 */
	public void writeToCSV(ArrayList<WordData> resultData, String outputPath) {
		
		Iterator<WordData> sortedDataIterator = resultData.iterator();
		WordData currRowValue = null;
		String unique = "";
		
		// Creating a new output file. In case of an issue occurs while creating the file,
		// below code will not be get executed.
		
		try (FileWriter csvFileWriter = new FileWriter(outputPath)) {
			
			// Setting header for the output file
			csvFileWriter.append(OUTPUT_FILE_HEADER);
			
			// Creating an iterator of sorted words.
			while(sortedDataIterator.hasNext()) {
				currRowValue = sortedDataIterator.next();
				
				// Adding a new line before printing expected output in to the file
				csvFileWriter.append("\n");
				
				// Checking and setting uniqueness
				unique = currRowValue.isUnique? "TRUE":"FALSE";
				
				// Writing current row data to file.[Word, Uniqueness, Weight]
				csvFileWriter.append(String.join(",", 
						currRowValue.wordValue, 
						unique, 
						String.valueOf(currRowValue.weight)));
			}
		} catch (IOException e) {
			System.out.println(String.join(" ",
			"Error occured while writing to csv. Details:",
			e.getMessage()));
		}
	}
}
